Saner handling of dual-use as user and system-wide daemon:
 1) Make default config usable both for user and system-wide daemon:
  * Stop hardcoding file-store. See config part of patch 2001.
 2) Improve default file-store path when used as user daemon:
  * Patch default file-store to be ~/.local/share/Radicale/collections
    (to mimic XDG_DATA_HOME).
    Bug: https://github.com/Kozea/Radicale/issues/153
  * Adjust NEWS.Debian file accordingly.
 3) Improve default file-store path when used as system-wide daemon:
  * Patch to allow overriding file-store path with command-line option,
    to not depend on $HOME for system-wide daemon setups.
    Bug: https://github.com/Kozea/Radicale/issues/328
  * Adjust NEWS.Debian file accordingly.
 4a) Keep radicale as-is, and introduce system-wide daemon separately:
  * Move sysV init script and packaging scripts into separate
    radicale-daemon package, and start daemon by default.
  * Tidy NEWS.Debian file accordingly.
 4b) Switch to system-wide daemon by default, but allow overriding:
  * Ask with debconf if daemon should be enabled at start (default yes).

Improved log rotation:
  * Implement reload action in init script.
  * Update logrotate script to reload (not restart) and include option
    "notifempty".

Improved testsuite:
  * Make use of python-caldav.
    See <https://github.com/Kozea/Radicale/issues/232>
    and <https://github.com/Kozea/Radicale/issues/330#issuecomment-270259730>
