# Apache2 snippet to serve Radicale CardDAV/CardDAV service via uWSGI
#
# example use:
#
#<VirtualHost *:443>
#	ServerName events.example.org
#
#	# optional path prefix, when serving several apps in one vhost
#	#Define _RADICALE_PREFIX /mycalendar
#
#	Include conf-available/radicale.conf
#
#	# reuse LocationMatch resolved in radicale snippet
#	<LocationMatch "${_RADICALE_LOCATION_MATCH}">
#		AuthType Basic
#		AuthName "Radicale: Authentication Required"
#		AuthUserFile "/etc/radicale/passwd"
#		AllowOverride None
#		Require valid-user
#	</LocationMatch>
#
#	# cleanup temporary variables
#	UnDefine _RADICALE_PREFIX
#	UnDefine _RADICALE_APP
#	UnDefine _RADICALE_LOCATION_MATCH
#
#	ErrorLog ${APACHE_LOG_DIR}/events.example.org-error.log
#	CustomLog ${APACHE_LOG_DIR}/events.example.org-access.log combined
#</VirtualHost>

# allow overriding which uWSGI socket to proxy into
<IfDefine !_RADICALE_APP>
	Define _RADICALE_APP radicale
</IfDefine>

# avoid Let's Encrypt validation path when not using _RADICALE_PREFIX
<IfDefine _RADICALE_PREFIX>
	Define _RADICALE_LOCATION_MATCH ^${_RADICALE_PREFIX}.*
</IfDefine>
<IfDefine !_RADICALE_PREFIX>
	Define _RADICALE_LOCATION_MATCH ^(?!/.well-known/acme-challenge).*
</IfDefine>

<LocationMatch "${_RADICALE_LOCATION_MATCH}">
	ProxyPass        unix:/run/uwsgi/app/${_RADICALE_APP}/socket|uwsgi://${_RADICALE_APP}
	ProxyPassReverse unix:/run/uwsgi/app/${_RADICALE_APP}/socket|uwsgi://${_RADICALE_APP}
	<IfDefine _RADICALE_PREFIX>
		RequestHeader set X-SCRIPT-NAME ${_RADICALE_PREFIX}
	</IfDefine>
</LocationMatch>

# smooth over proxy being strict about path separator
<IfDefine _RADICALE_PREFIX>
	RedirectMatch permanent ^${_RADICALE_PREFIX}$ ${_RADICALE_PREFIX}/
</IfDefine>
