# example apache2 vhost snippet for Radicale CalDAV/CardDAV service

Define _DOMAIN example.org
Define _HOST event.${_DOMAIN}

# secure-only access to protect password
# assumes already configured TLS certificates and mod-ssl / mod-gnutls
<VirtualHost *:443>
        ServerName ${_HOST}
        ServerAdmin webmaster@${_DOMAIN}
        DocumentRoot /var/www/nowhere

        Include conf-available/radicale.conf

        # authenticate against PAM with mod-authnz-external
        # reusing LocationMatch resolved in radicale snippet
        DefineExternalAuth pwauth pipe /usr/sbin/pwauth
        <LocationMatch "${_RADICALE_LOCATION_MATCH}">
                AuthType Basic
                AuthName "Radicale: Authentication Required"
                AuthBasicProvider external
                AuthExternal pwauth
                AllowOverride None
                Require valid-user
        </LocationMatch>

        ErrorLog ${APACHE_LOG_DIR}/${_HOST}-error.log
        CustomLog ${APACHE_LOG_DIR}/${_HOST}-access.log combined
</VirtualHost>

# insecure access redirects to secure
<VirtualHost *:80>
        ServerName ${_HOST}
        ServerAdmin webmaster@${_DOMAIN}
        DocumentRoot /var/www/nowhere

        # avoid Let's Encrypt validation path
        RedirectMatch permanent ^(?!/.well-known/acme-challenge)(.*) https://${_HOST}/$1

        CustomLog ${APACHE_LOG_DIR}/redirect.log vhost_combined
</VirtualHost>

# cleanup temporary variables
UnDefine _RADICALE_LOCATION_MATCH
UnDefine _RADICALE_APP
UnDefine _RADICALE_PREFIX
UnDefine _HOST
UnDefine _DOMAIN
