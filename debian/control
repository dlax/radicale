Source: radicale
Section: web
Priority: optional
Maintainer: Jonas Smedegaard <dr@jones.dk>
Uploaders:
 Martin Stigge <martin@stigge.org>,
 Christian M. Amsüss <chrysn@fsfe.org>,
Standards-Version: 4.4.0
Build-Depends:
 cdbs,
 debhelper,
 dh-python,
 help2man,
 python3,
 python3-atomicwrites,
 python3-bcrypt <!nocheck>,
 python3-dateutil (>= 2.7.3),
 python3-passlib <!nocheck>,
 python3-pkg-resources,
 python3-pytest <!nocheck>,
 python3-setuptools,
 python3-tz,
 python3-vobject (>= 0.9.6),
Vcs-Browser: https://salsa.debian.org/debian/radicale
Vcs-Git: https://salsa.debian.org/debian/radicale.git
Homepage: https://radicale.org/
Rules-Requires-Root: no

Package: radicale
Architecture: all
Depends:
 adduser,
 lsb-base,
 python3,
 python3-radicale (= ${binary:Version}),
 ${misc:Depends},
 ${python:Depends},
Recommends:
 ssl-cert,
Suggests:
 apache2,
 apache2-utils,
 libapache2-mod-proxy-uwsgi,
 python3-bcrypt,
 python3-passlib,
 uwsgi,
 uwsgi-plugin-python3,
Provides:
 ${python:Provides},
Description: simple calendar and addressbook server - daemon
 Radicale is a CalDAV (calendar) and CardDAV (contact) server.
 .
 Calendars and address books are available for both local and remote
 access, possibly limited through authentication policies. They can be
 viewed and edited by calendar and contact clients on mobile phones or
 computers.
 .
 This package contains the radicale daemon.
 .
 Creating encrypted password files require the package apache2-utils.
 .
 Stronger password hashes require the packages python3-passlib and
 python3-bcrypt.
 .
 Serving directly with uWSGI
 requires the packages uwsgi and uwsgi-plugin-python3.
 Serving with Apache as front-end and uWSGI only as middleware
 additionally requires the packages apache2 and libapache2-mod-proxy-uwsgi.

Package: python3-radicale
Section: python
Architecture: all
Depends:
 python3-atomicwrites,
 python3-dateutil (>= 2.7.3),
 python3-pkg-resources,
 python3-tz,
 python3-vobject (>= 0.9.6),
 ${misc:Depends},
 ${python3:Depends},
Provides:
 ${python3:Provides},
Description: simple calendar and addressbook server - python3 module
 Radicale is a CalDAV (calendar) and CardDAV (contact) server.
 .
 Calendars and address books are available for both local and remote
 access, possibly limited through authentication policies. They can be
 viewed and edited by calendar and contact clients on mobile phones or
 computers.
 .
 This package contains the Radicale Python 3.x module.
